from PIL import ImageOps, ImageEnhance


def gray_scale(img):
    try:
        gray = ImageOps.grayscale(img)
        enhancer = ImageEnhance.Contrast(gray)
        im_output = enhancer.enhance(1.2)
        enhancer2 = ImageEnhance.Brightness(im_output)
        img = enhancer2.enhance(1.1)
        return img
    except Exception:
        return 500
