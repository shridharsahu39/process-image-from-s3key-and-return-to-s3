import cv2
from resources import raw_image_to_cv2


face_cascade = cv2.CascadeClassifier(
    f"{cv2.data.haarcascades}haarcascade_frontalface_default.xml"
)


def is_Face(bucket, s3Uri):
    try:
        read_image = raw_image_to_cv2(bucket, s3Uri)
        grayImage = cv2.cvtColor(read_image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(grayImage)
        return 0 if len(faces) == 0 else 1
    except Exception:
        return 500
