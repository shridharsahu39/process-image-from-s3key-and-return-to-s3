import boto3
from config import S3_BUCKET, S3_KEY, S3_SECRET
from flask import session
import cv2
import os
import numpy as np
from io import BytesIO
from PIL import Image


def _get_s3_resource():
    if S3_KEY and S3_SECRET:
        return boto3.resource(
            "s3", aws_access_key_id=S3_KEY, aws_secret_access_key=S3_SECRET
        )
    else:
        return boto3.resource("s3")


def get_bucket():
    s3_resource = _get_s3_resource()
    bucket = session["bucket"] if "bucket" in session else S3_BUCKET
    return s3_resource.Bucket(bucket)


s3 = _get_s3_resource()


def readS3Object(_bucket, _key):
    obj = s3.Object(_bucket, _key)
    return obj.get()["Body"].read()


def put_object_in_s3(bucket_name, processed_s3_path, processed_image):
    return s3.Object(bucket_name, processed_s3_path).put(Body=processed_image)


def create_raw_body(object):
    buffer = BytesIO()
    object.save(buffer, format="PNG")
    return buffer.getvalue()


def read_raw_image(object):
    return Image.open(BytesIO(object))


def raw_image_to_cv2(bucket, key):
    nparr = np.frombuffer(readS3Object(bucket, key), np.uint8)
    return cv2.imdecode(nparr, cv2.IMREAD_COLOR)


def download_s3_object(object_key):
    try:
        s3.Bucket(S3_BUCKET).download_file(
            object_key, f"s3Images/{os.path.basename(object_key)}"
        )
        return object_key
    except Exception:
        return 0


def upload_to_s3(local_file_path, object_key):
    try:
        s3.Object(S3_BUCKET, object_key).upload_file(local_file_path)
        return 1
    except Exception:
        return 0
