import contextlib
import requests
import os
from config import CUT_OUT_PRO_API_KEY
from resources import download_s3_object, upload_to_s3


def remove_all_local_images(name):
    with contextlib.suppress(Exception):
        os.remove(f"s3Images/{name}")
        os.remove(f"miniature/{name}")
        os.remove(f"FaceCutImg/{name}")


def face_cut(name):
    try:
        response = requests.post(
            "https://www.cutout.pro/api/v1/matting?mattingType=3&crop=true&preview=true",
            files={"file": open(f"miniature/{name}", "rb")},
            headers={"APIKEY": CUT_OUT_PRO_API_KEY},
        )
        with open(f"FaceCutImg/{name}", "wb") as out:
            out.write(response.content)
        return True
    except Exception:
        return False


def cartoon(name):
    try:
        response = requests.post(
            "https://www.cutout.pro/api/v1/cartoonSelfie?cartoonType=1",
            files={"file": open(f"s3Images/{name}", "rb")},
            headers={"APIKEY": CUT_OUT_PRO_API_KEY},
        )
        with open(f"miniature/{name}", "wb") as out:
            out.write(response.content)
        return name
    except Exception:
        return False


def miniature_face(url):
    try:
        key = download_s3_object(url)
        if key == 0:
            return 0
        name = os.path.basename(key)
        img_name = cartoon(name)
        if img_name == False:
            return 0

        status = face_cut(img_name)
        if status == False:
            return 0

        res = upload_to_s3(f"FaceCutImg/{name}", f"{key}_miniature")
        if res == 1:
            remove_all_local_images(name)
        return f"{key}_miniature"
    except Exception:
        remove_all_local_images(name)
        return 0
