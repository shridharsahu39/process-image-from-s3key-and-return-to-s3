# Start with a base Python 3.9 image
FROM python:3.9

# Install build tools and libraries
RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get install -y libgl1-mesa-glx


# Set the working directory
WORKDIR /app

# Copy the requirements file to the container
COPY requirements.txt .

# Install the Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application files to the container
COPY . .

# Expose the port
EXPOSE 5000

# Start the Flask application using Gunicorn
CMD ["python", "app.py"]
