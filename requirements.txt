flask
flask_cors
botocore
boto3
numpy
pillow
opencv-python
opencv-contrib-python
rembg
gevent
gunicorn
python-dotenv

