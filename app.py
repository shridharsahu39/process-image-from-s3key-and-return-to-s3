from flask import Flask, request, jsonify, make_response
from flask_cors import CORS
import gevent.pywsgi
from rembg import remove
from resources import (
    get_bucket,
    put_object_in_s3,
    create_raw_body,
    read_raw_image,
    readS3Object,
)
from miniature_cut import miniature_face
from grayScale import gray_scale
from faceDetection import is_Face


app = Flask(__name__)
CORS(app)


@app.route("/api/faceDetection", methods=["POST"])
def face_detect():
    jsonObject = request.get_json()
    my_bucket = get_bucket()

    formatted_url = jsonObject["image_urls"]

    non_human_face = []

    for url in formatted_url:
        try:
            is_face = is_Face(my_bucket.name, url)
            if is_face == 500:
                return make_response(jsonify({"message": "file not found"}), 404)
            if is_face == 0:
                non_human_face.append(formatted_url.index(url) + 1)
        except Exception:
            return make_response(jsonify({"message": "file not found"}), 404)

    return (
        jsonify(
            {
                "message": "There are non human faces for image in sequence no "
                + ",".join(str(x) for x in non_human_face)
            }
        )
        if non_human_face
        else jsonify({"message": "success"})
    )


@app.route("/api/grayScale", methods=["POST"])
def gyay_scale():
    mybucket = get_bucket()
    bucket_name = mybucket.name
    s3_path = request.get_json()
    processed_urls = []
    for key in s3_path["image_urls"]:
        try:
            image_content = readS3Object(bucket_name, key)
        except Exception:
            return make_response(jsonify({"message": "file not found"}), 404)
        image = read_raw_image(image_content)
        gray_image = gray_scale(image)
        if gray_image == 500:
            return make_response(jsonify({"message": "file not found"}), 404)
        processed_s3_path = f"{key}_grayScale"
        put_object_in_s3(bucket_name, processed_s3_path, create_raw_body(gray_image))
        processed_urls.append(processed_s3_path)
    return jsonify({"processed_images": processed_urls})


@app.route("/api/bgRemove", methods=["POST"])
def remove_bg():
    mybucket = get_bucket()
    bucket_name = mybucket.name
    s3_path = request.get_json()
    processed_urls = []

    for key in s3_path["image_urls"]:
        try:
            image_content = readS3Object(bucket_name, key)
        except Exception:
            return make_response(jsonify({"message": "file not found"}), 404)
        image = read_raw_image(image_content)
        bg_rem_img = remove(image)
        processed_s3_path = f"{key}_bgRemove"
        put_object_in_s3(bucket_name, processed_s3_path, create_raw_body(bg_rem_img))
        processed_urls.append(processed_s3_path)
    return jsonify({"processed_images": processed_urls})


@app.route("/api/miniature", methods=["POST"])
def miniature():
    s3_path = request.get_json()
    processed_urls = []

    for key in s3_path["image_urls"]:
        try:
            processed_s3_path = miniature_face(key)
            if processed_s3_path == 0:
                return make_response(jsonify({"message": "file not found"}), 404)
            else:
                processed_urls.append(processed_s3_path)
        except Exception:
            return make_response(jsonify({"message": "file not found"}), 404)
    return jsonify({"processed_images": processed_urls})


# dev server
# if __name__ == "__main__":
#     # _port = int(os.environ.get('PORT'))
#     app.run(debug=True, host="0.0.0.0", port=5000)

# prod server

if __name__ == "__main__":
    app_server = gevent.pywsgi.WSGIServer(("0.0.0.0", 5000), app)
    app_server.serve_forever()
