

## Introduction 
The processing of the image includes

Background removal
Greyscale formatting and
Face detection


The project intends at taking an image as input from s3 and processing it and saving the new image back to s3

To read the image from s3 we require some basic values viz.,
aws_access_key, aws_secret_key, bucket_name, key

aws_acess_key, aws_secret_key can be generated for either a specific folder or full access, we are currently accessing full access keys as the requirement for image processing could be in any of the buckets.

We have currently fixed the bucket as “static-bucket” for all image processing as we currently have the requirement only.



The project is developed using the python-flask framework. The basic structure of the framework is 

└── flask_app
    ├── app
    │   ├── extensions.py
    │   ├── __init__.py
    │   ├── main
    │   │   ├── __init__.py
    │   │   └── routes.py
    │   ├── models
    │   │   ├── post.py
    │   │   └── question.py
    │   ├── posts
    │   │   ├── __init__.py
    │   │   └── routes.py
    │   ├── questions
    │   │   ├── __init__.py
    │   │   └── routes.py
    │   └── templates
    │       ├── base.html
    │       ├── index.html
    │       ├── posts
    │       │   ├── categories.html
    │       │   └── index.html
    │       └── questions
    │           └── index.html
    ├── app.db
    └── config.py

app.py is the main executable file and all the other peripheral files are called/ executed internally through app.py, calling the required function from the required file at the given case/instance.

To run the app.py the command is 
python app.py


The project is deployed using docker, so build the docker image by configuring the Dockerfile in the project directory.


